'use strict';

/**
 * @ngdoc function
 * @name sampleApp1App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sampleApp1App
 */
/*globals Global: true*/

angular.module('sampleApp1App')
  .controller('MainCtrl',  ['$scope', '$rootScope', '$location', '$window', 'AuthenticationService',
    function ($scope, $rootScope, $location, $window, AuthenticationService ) {
        // reset login status
       AuthenticationService.ClearCredentials();

        $scope.login = function () {
           $scope.dataLoading = false;
            AuthenticationService.Login($scope.username, $scope.password, function(response) {
                if(response.success) {

                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    Global.loginUsername.USERNAME = $scope.username;
                    $location.path('/dashboard');
                    $scope.dataLoading = false;
     			}
     			else {
                    $scope.error = response.message;
                    $scope.dataLoading = true;
                }
            });
        };
      $scope.register = function () {
       $scope.registerParent = true;
      };

    }])
    .config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/login', {
            cache: false,
            controller: 'MainCtrl',
            templateUrl: 'views/main.html'
        })
      .when('/parent', {
        controller: 'parentCtrl',
        cache: false,
        templateUrl: 'views/Parent_HomePage.html'
      })
        .when('/dashboard', {
            cache: false,
            controller: 'dashboardCtrl',
            templateUrl: 'views/dashboard.html'
        })
        .when('/dashboard/1', {
            cache: false,
            controller: 'dashboardCtrl',
            templateUrl: 'views/dashboard.html'
        })
        .when('/group', {
            cache: false,
            controller: 'groupCtrl',
            templateUrl: 'views/AddtoStudyGroup.html'
        })
        .when('/record', {
            cache: false,
            controller: 'recordCtrl',
            templateUrl: 'views/record.html'
        })
         .when('/child', {
           cache: false,
            controller: 'childCtrl',
            templateUrl: 'views/Childs_Home_Page.html'
        })
      .when('/register', {
        cache: false,
        controller: 'registerCtrl',
        templateUrl: 'views/register.html'
      })
      .when('/registerChild', {
        cache: false,
        controller: 'registerChildCtrl',
        templateUrl: 'views/Register_Your_Children.html'
      })
      .when('/edit', {
        cache: false,
        controller: 'editCtrl',
        templateUrl: 'views/edit.html'
      })
      .when('/chapterPage', {
        cache: false,
        controller: 'chapterPageCtrl',
        templateUrl: 'views/chapter_page.html'
      })
      .when('/topics', {
        cache: false,
        controller: 'topicsCtrl',
        templateUrl: 'views/Topics.html'
      })
      .when('/topics2', {
        cache: false,
        controller: 'topicsCtrl',
        templateUrl: 'views/Topics2.html'
      })
      .when('/rec_dashboard', {
        cache: false,
        controller: 'recDashboardCtrl',
        templateUrl: 'views/rec_dashboard.html'
      })
      .when('/uploadaudio', {
        cache: false,
        controller: 'uploadaudioCtrl',
        templateUrl: 'views/uploadaudio.html'
      })
      .when('/hello', {
        cache: false,
        controller: 'helloCtrl',
        templateUrl: 'views/hello.html'
      })
      .otherwise({ redirectTo: '/login' });
    }])

.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {

        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common.Authorization = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }
        $rootScope.$on('$locationChangeStart', function () {

            // redirect to login page if not logged in
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                if($rootScope.registerParent === true){
                  $location.path('/register');
                }

              $location.path('/login');
            }

        });
    }]);

