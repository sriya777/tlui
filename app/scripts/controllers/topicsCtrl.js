'use strict';

/**
 * @ngdoc function
 * @name sampleApp1App.controller:topicsCtrl
 * @description
 * # topicsCtrl
 * Controller of the sampleApp1App
 */
/*globals Global: true*/

angular.module('sampleApp1App')
  .controller('topicsCtrl',['$scope','$http','UserService',function ($scope, $http, UserService){
        var fetch = {};
        var chId = sessionStorage.getItem("chapterId");
        var userId=sessionStorage.getItem("userId");
        //var username=Global.loginUsername.USERNAME ;
        var username = sessionStorage.getItem("username");
        var classId =Global.requiredId.CLASSID;
        var shareData = {};
        var shareUserId = {};
        $scope.user = sessionStorage.getItem("username");
       // $scope.rmv = true;
    sessionStorage.setItem("audioId", Global.requiredId.AUDIOID);

    UserService.fetchAllUsers(Global.RestUrls.audioUrl,sessionStorage.getItem("audioId"))
      $scope.refresh = function(){

        event.preventDefault();
        // $event.stopPropagation();
        location.reload();
        // $scope.removebutton(fetch);
        // UserService.fetchAllUsers(Global.RestUrls.topicUrl,username)
        //   .then(function(response){
        //     $scope.topicslist = response;
        //     console.log(response);
        //   });

      };

      // UserService.fetchAllUsers(Global.RestUrls.topicUrl,sessionStorage.getItem("chapterId"))
    UserService.fetchUser(Global.RestUrls.topicUrl,sessionStorage.getItem("chapterId"),Global.RestUrls.withUserUrl,userId)
      .then(function(response){
        $scope.topicslist = response;
        fetch = $scope.topicslist;
        console.log(fetch);
        // $scope.removebutton(fetch);
      });

      UserService.fetchAllUsers(Global.RestUrls.shareUrl,userId)
        .then(function (response) {
          $scope.sharelist=response;
          shareUserId =  $scope.sharelist;

        });

      $scope.removeAudio=function(audioId){
        UserService.fetchAllUsers(Global.RestUrls.removeUrl,audioId)
          .then(function(response){
            $scope.removeaudio=response;
          });
        $scope.refresh();
      };

      $scope.removebutton = function(fetch){
        angular.forEach(fetch,function (value,key) {
          if (value.source === username) {
            $scope.rmv = false;
          }
          else{
            $scope.rmv = true;
          }
        });
      };

      $scope.shareAudio=function(id){
        //passed audioId to modal
        //alert(id);

        UserService.fetchAllUsers(Global.RestUrls.audioUrl,id)
          .then(function(response){
            $scope.audioList=response;
            shareData = $scope.audioList;
            console.log(shareData);
          });
        // $scope.removebutton(fetch);
      };

       $scope.deleteAudio = function(id){
        //alert(id);
        if (confirm("Do you want to delete this recording?")) {
            UserService.deleteUser(id);
            $scope.refresh();
        }
        return false;
       };

      console.log("Loaded");
      $scope.timeLimit = 10;

      //var elapsedTime = $scope.elapsedTime;


      $scope.insertAudio = function(){
        var data = {chapterId: chId, userId: userId, source: username, topicName: $scope.topicname, reference:$scope.reference, comments:$scope.comments};
        //var data = {topicName: $scope.topicname}
        //var data = $scope.topicname;
        console.log(data);
        UserService.updateUser(Global.RestUrls.insUrl,data);
      };

      $scope.insertSharedAudio=function () {
        angular.forEach(shareUserId,function (value,key){
          var data={chapterId: chId, source: username, topicName: shareData[0].topicName, reference:shareData[0].reference, comments:shareData[0].comments, userId:value.userId,length: shareData[0].length,listenCount:shareData[0].listenCount};
          UserService.updateUser(Global.RestUrls.sharedUrl,data);
        });

        //var data={chapterId: chId, source: username, topicName: shareData[0].topicName, reference:shareData[0].reference, comments:shareData[0].comments, userId:shareUserId[0].userId,length: shareData[0].length,listenCount:shareData[0].listenCount};
        //console.log(data);.
        //var data = shareData[0];

        // $scope.removebutton(fetch);
      };

      $scope.collapseOne = false;

      $scope.test = function(){
        $scope.collapseOne = !$scope.collapseOne;

      };
      $scope.toggle_visibility = function () {
        $scope.RecentlyListenedsub = !$scope.RecentlyListenedsub ;
      };
      $scope.toggle_latest = function(){
        $scope.MyLatestRecordingssub = !$scope.MyLatestRecordingssub ;
      };
      $scope.toggle_shared = function(){
        $scope.RecentlySharedWithMesub = !$scope.RecentlySharedWithMesub ;
      };
    // $scope.getAll = function(){
    //   var url="http://10.11.0.31:8080/audio/all";
    //   $http.get(url).(function(response){
    //     $scope.recordings=response.data[0];
    //     console.log(recordings);
    //   });
    // };
    //$scope.getAll();
    // $window.onbeforeunload = function() {
    //   var hack = /irefox\/([4-9]|1\d+)/.test(navigator.userAgent);
    //   if (hack)
    //     alert( warning + '\n\n(Pardon the double dialogs ' + 'caused by Firefox bug 588292.)');
    //   return warning;}
    // };

  }])
  .config(function (recorderServiceProvider) {
    recorderServiceProvider
      .forceSwf(false)
      .setSwfUrl('/lib/recorder.swf')
      .withMp3Conversion(true);
  });
